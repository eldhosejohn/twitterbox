from django.conf.urls.defaults import patterns, include, url
from twitter.views import start, fetch,search

urlpatterns = patterns('',
    url(r'^new/$', start),
    url(r'^fetch/$', fetch),
    url(r'^search/', search),
    );