from django.db import models

# Create your models here.

class Tweet(models.Model):
    text = models.CharField(max_length=140)
    user = models.CharField(max_length=50)
    