# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponse

from django.views.decorators.csrf import csrf_exempt

from twython import Twython
import json

#from pyelasticsearch import *

from pyes import *
from pyes.mappings import *

@csrf_exempt
def fetch(request):
    conn = ES('127.0.0.1:9200')
    
    try:
        conn.indices.create_index("test-index4")
    except:
        pass

    screen_name = request.GET.get("screen_name")
    t = Twython(app_key="KKwEIBHXNqXuuqcbQeK1XQ", app_secret="0WC6780vH4FMMpFMCj0wc0BjHTAPd0ckRQbGCxfKc",oauth_token="24859161-fuHIM1AG80w7dWvjW6WwckIRW3pQr6xXlBQuErZVd",oauth_token_secret="shspj2PqkUxayrmmcfc9eIezA1WEO2P5gvn1l7hXz8")
    #t = Twython()
    
    user_timeline = t.getUserTimeline(screen_name=screen_name)
 
    tweets = list()
    for tweet in user_timeline:
        
        tweet_id =  tweet["id"]
        tweet_text = tweet["text"]
        screen_name = tweet["user"]["screen_name"]
        
        dict1 = dict()
        dict1["text"] =tweet_text
        dict1["screen_name"] = screen_name

        tweet1 = json.dumps(dict1)
        
        conn.index(tweet1, "test-index4","tweet",tweet_id)
        
        tweets.append(dict1)
 
    return HttpResponse(json.dumps(tweets))


@csrf_exempt
def start(request):
    return render_to_response('home.html', { })


@csrf_exempt
def search(request):
    conn = ES('127.0.0.1:9200')
    
    tweets = list()
    
    screen_name = request.GET.get("screen_name")
    search_query = request.GET.get("q")
    
    q1 = TermQuery("text", search_query)
    q2 =TermQuery("screen_name",screen_name)
    
    query = BoolQuery(must=[q1,q2])
    
    results = conn.search(query)
    
       
    for result in results:
        dict1 = dict()
        dict1["text"] = result["text"]
        
        tweets.append(dict1)
    
    return HttpResponse(json.dumps(tweets))
        
        